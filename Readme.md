# Activities for your vacations!

This API will provide some activities for your vacations. So far it only supports Madrid.
 
# Installation

Clone this repository and enter on its directory.

Create a virtual environment:

    $ virtualenv venv

Activate it:

    $ source venv/bin/activate

Install the requirements:

    $ pip install -r requirements.txt

Run the app:

    $ flask run

Enjoy your API (still in development!) in [http://localhost:5000/api/v1/](http://localhost:5000/api/v1/)