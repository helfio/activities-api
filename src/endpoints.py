from flask import Blueprint, jsonify, request, abort
import re
from . import tools

# Activities blueprint
activities = Blueprint(name="activities", import_name=__name__)

# Making data accessible
cities, data = tools.loadData()

# Views
# Provides the list of cities with some activity
@activities.route('/')
def root():
    return jsonify(cities)

# Provides the list of activities of a given city
@activities.route('/<string:city>/')
def viewActivities(city):
    return jsonify(tools.customCity(data, city))

# Provides the list of activities filtered by category, location or district
@activities.route('/<string:city>/<string:filter>/', methods=['GET'], defaults={'value': ''})
@activities.route('/<string:city>/<string:filter>/<string:value>/', methods=['GET'])
def viewFilteredActivities(city, filter, value):
    if filter not in ['category', 'location', 'district']:
        abort(404)
    if value == "":
        return viewActivities(city)
    customData = tools.customCity(data, city)
    return jsonify(tools.filteredDataBy(customData, filter, value))

# Provides a recommended activity based on category, location or district and a time range
# which is expected to be in the same format available in the JSON file (i.e. 09:00-12:00)
@activities.route('/<string:city>/<string:filter>/<string:value>/recommend/<string:time_range>/', methods=['GET'])
def viewRecommendedActivity(city, filter, value, time_range):
    if filter not in ['category', 'location', 'district']:
        abort(404)
    # RegExp check for correct time range format
    if not re.fullmatch('[0-9]*:[0-9]*-[0-9]*:[0-9]*',time_range):
        abort(404)
    customData = tools.customCity(data, city)
    myFilteredData = tools.filteredDataBy(customData, filter, value)
    return jsonify(tools.recommendActivity(myFilteredData, time_range))
