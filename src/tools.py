from os import walk, path
from datetime import date, datetime, timedelta
import json

# Load data from JSON files into memory
def loadData():
    geojson_data = {}
    for root,folders,files in walk('cities'):
        for cityFile in files:
            with open(path.join('cities',cityFile), 'r') as f:
                raw_data = json.load(f)
                city = cityFile.split('.')[0]
                geojson_data[city] = []
                for item in raw_data:
                    # According to RFC 7946 (GeoJSON specs)
                    new_item = {'type': 'Feature',
                                'geometry': {'type': 'Point',
                                             'coordinates': [x for x in reversed(item['latlng'])]},
                                'properties': item}
                    geojson_data[city].append(new_item)
    return [x.lower() for x in geojson_data.keys()],geojson_data

# Returns list of activities from a given city
def customCity(data, city):
    return data[city]

# Returns filtered activities by category, district or location
def filteredDataBy(input_data, filter_key, filter_value=None):
    if not filter_value:
        return input_data
    output = []
    for item in input_data:
        if item['properties'][filter_key].lower() == filter_value.lower():
            output.append(item)
    if output == []:
        return {'error': 'no activities match that criteria'}
    return output

# Returns a recommended activity based on the previous filter and the given time range
def recommendActivity(input_data, time_range):
    # Using current day of the week
    week_day = date.today().strftime('%a').lower()[:2]
    # Empty list to store possible activities
    possible_activities = []
    # Creating start and end datetime objects
    start_datetime = datetime.now().replace(hour=int(time_range.split('-')[0].split(':')[0]), 
                                        minute=int(time_range.split('-')[0].split(':')[1]),
                                        second=0,
                                        microsecond=0)
    end_datetime = datetime.now().replace(hour=int(time_range.split('-')[1].split(':')[0]), 
                                        minute=int(time_range.split('-')[1].split(':')[1]),
                                        second=0,
                                        microsecond=0)
    # Loop over available activities
    for possibility in input_data:
        # Today is opened
        if len(possibility['properties']['opening_hours'][week_day]) > 0:
            # Saving opening_hours
            opening_hours = possibility['properties']['opening_hours'][week_day][0]
            # Creating open and close datetime objects
            open_datetime = datetime.now().replace(hour=int(opening_hours.split('-')[0].split(':')[0]), 
                                        minute=int(opening_hours.split('-')[0].split(':')[1]),
                                        second=0,
                                        microsecond=0)
            close_datetime = datetime.now().replace(hour=int(opening_hours.split('-')[1].split(':')[0]), 
                                        minute=int(opening_hours.split('-')[1].split(':')[1]),
                                        second=0,
                                        microsecond=0)
            # Creating timedelta object
            duration = timedelta(hours=possibility['properties']['hours_spent'])
            # It's opened by the begining of the availability hours
            if open_datetime <= start_datetime:
                # It's still opened after spending the hours_spent
                if open_datetime+duration <= close_datetime:
                    # User have enough time to cover hours_spent
                    if open_datetime+duration <= end_datetime:
                        possible_activities.append(possibility)
            
    # Keeping only the longest activity in case there is several.
    if len(possible_activities) > 1:
        longest_activity = {'properties':{'hours_spent':0}}
        for possibility in possible_activities:
            if possibility['properties']['hours_spent'] > longest_activity['properties']['hours_spent']:
                longest_activity = possibility
        return longest_activity
    return possible_activities
