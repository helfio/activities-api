from flask import Flask, jsonify

# Load blueprints
from src.endpoints import activities

# Init Flask app
app = Flask(__name__)

# Register blueprints
app.register_blueprint(activities, url_prefix="/api/v1/")
